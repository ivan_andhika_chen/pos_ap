<?php

class CategoriesController extends BaseController {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $data = array(
    'categories' => ItemCategory::paginate(10)
      );
    return View::make("categories.index", $data);
  }

  public function trash()
  {
    $data = array(
    'categories' => ItemCategory::onlyTrashed()->paginate(10)
      );
    return View::make("categories.trash", $data);
  }

  public function restore()
  {
    $id = Input::get('id');
    if(empty($id)) return "false";
    $item_category = ItemCategory::onlyTrashed()->find($id);
    if($item_category->restore()) return "true";
    return "false";
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    $category = new ItemCategory;
    $category->name = Input::get('name');

    if ($category->save()) return ItemCategory::find($category->id)->toJson();
    return serialize(json_decode($category->errors));
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    $post = new ItemCategory(Input::all());
    $category = ItemCategory::find($post->id);
    $category->name = $post->name;
    if ($category->save()) return "success";
    return serialize(json_decode($category->errors));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $category = ItemCategory::find($id);
    if(empty($category)) return "true";
    if($category->delete()) return "true";
    return "false";
  }

}