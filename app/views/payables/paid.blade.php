@extends('layouts.admin')
@section('style')
  <style type="text/css">
    #payable_body tr td input,#payable_body tr td select{
      width: 100px;
      margin : 0px;
    }
    #payable_body tr td textarea{
      width: 150px;
      height: 100px;
    }
    #payable_body tr td.stock input,#payable_body tr td.real_stock input{
      width: 40px;
      margin : 0px;
    }
    .actions{
      width: 130px;
    }
  </style>
@stop
@section('main')
  <div class="row-fluid">
    <div class="span9">
      <h3>List Paid Transactions &nbsp;
        <!-- <button id="add_item" class="btn btn-mini btn-primary" type="button" onClick="createNew();" style="<?= $errors!='[]' ? 'display:none':'' ?>"><i class="icon-pencil icon-white"></i> Add</button> -->
      </h3>
    </div>
    <div class="span3">
      <h3 style="text-align:right;">
        &nbsp;<a id="paid" class="btn btn-mini btn-success" href="/payables"><i class="icon-book icon-white"></i> Payables</a>
      </h3>
    </div>
  </div>

  <table class="table table-striped table-bordered" style="background:white;">
    <thead>
      <tr>
        <th>No.</th>
        <th>Number</th>
        <th>Date</th>
        <th>Employee name</th>
        <th>Customer name</th>
        <th>Payment Date</th>
        <th>Grand Total </th>
        <th>Created</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody id="payable_body">
      @if(count($payables)>0)
        @foreach ($payables as $key => $payable)
          <tr id="row_{{$payable->id}}">
            <td class="no"><?=($key+1) ?></td>
            <td class="editable number" >
              <span>{{$payable->number; }}</span>
            </td>
            <td class="editable date" >
              <span>{{$payable->date; }}</span>
            </td>
            <td class="editable employee_id" >
              <span>{{$payable->employee->name; }}</span>
            </td>
            <td class="editable customer_id" >
              <span>{{$payable->customer->name; }}</span>
            </td>
            <td class="editable payment_date" >
              <span>{{$payable->payment_date; }}</span>
            </td>
            <td class="editable grand_total" >
              <span>{{$payable->grand_total; }}</span>
            </td>
            <td>{{date("d-m-Y",strtotime($payable->created_at)); }}</td>
            <td class="actions">
              {{ Form::button('<i class="icon-th-list icon-white"></i> Detail', array('class'=>'edit btn btn-mini btn-info')) }}
              {{ Form::button('<i class="icon-remove icon-white"></i> Delete', array('class'=>'delete btn btn-mini btn-danger','onClick'=>"confirmDelete('".$payable->id."');")) }}

              {{ Form::button('<i class="icon-ok icon-white"></i> Update', array('class'=>'update btn btn-mini btn-success','style'=>'display:none;','onClick'=>"update('".$payable->id."')")) }}
              {{ Form::button('Cancel', array('class'=>'cancel_update btn btn-mini','style'=>'display:none;','onClick'=>"cancelUpdate('".$payable->id."');")) }}

              {{ Form::button('<i class="icon-remove icon-white"></i> Delete', array('class'=>'confirmDelete btn btn-mini btn-danger','style'=>'display:none;','onClick'=>"deleteItem('".$payable->id."');")) }}
              {{ Form::button('Cancel', array('class'=>'cancelDelete btn btn-mini','style'=>'display:none;','onClick'=>"confirmDelete('".$payable->id."');")) }}
            </td>
          </tr>
        @endforeach
      @else
        <tr class="info">
          <td colspan="12"><p class="text-center">No Data</p></td>
        </tr>
      @endif
    </tbody>
  </table>
  {{ $payables->links() }}
@stop

@section('script')
  <script type="text/javascript">
    $(document).ready(function(){
      numberOnly(".number")
      numberOnly(".decimal")
    })
    function createNew(){
      $('#form_add_item').slideDown();
      $('#add_item').slideUp();
    }

    function cancelNew(){
      $('#form_add_item').slideUp();
      $('#add_item').slideDown();
    }

    $(".editable").click(function(){
      $(this).children("input").show();
      $(this).children("textarea").show();
      $(this).children("select").show();
      $(this).children("span").hide();
      $(this).siblings(".actions").children('.edit').hide();
      $(this).siblings(".actions").children('.delete').hide();
      $(this).siblings(".actions").children('.update').show();
      $(this).siblings(".actions").children('.cancel_update').show();
    })

    function cancelUpdate(row_id){
      $("#row_"+row_id+" > td").each(function(){
        $(this).children("span").show();
        $(this).children("input").hide();
        $(this).children("textarea").hide();
        $(this).children("select").hide();
      })
        $("#row_"+row_id).children(".actions").children('.edit').show();
        $("#row_"+row_id).children(".actions").children('.delete').show();
        $("#row_"+row_id).children(".actions").children('.update').hide();
        $("#row_"+row_id).children(".actions").children('.cancel_update').hide();
    }

    function updateAjax(row_id){
      $("#row_"+row_id+" > td").each(function(){
        if ($(this).children("input").length >0){
          $(this).children("span").text($(this).children("input").val());
        }
        if ($(this).children("textarea").length >0){
          $(this).children("span").text($(this).children("textarea").val());
        }
        if ($(this).children("select").length >0){
          $(this).children("span").text($(this).children("select").text());
        }
        $(this).children("span").show();
        $(this).children("input").hide();
        $(this).children("textarea").hide();
        $(this).children("select").hide();
      })
      $("#row_"+row_id).children(".actions").children('.edit').show();
      $("#row_"+row_id).children(".actions").children('.delete').show();
      $("#row_"+row_id).children(".actions").children('.update').hide();
      $("#row_"+row_id).children(".actions").children('.cancel_update').hide();
    }

    function update(item_id){
      $.ajax({
          url: '/items/'+item_id,
          type: 'put',
          data: {_token: "{{csrf_token()}}", id : item_id, barcode : $(".barcode_"+item_id).val(), name : $(".name_"+item_id).val(), description : $(".description_"+item_id).val(), item_category_id : $(".item_category_"+item_id).val(), vendor_id : $(".vendor_"+item_id).val(), stock : $(".stock_"+item_id).val(), real_stock : $(".real_stock_"+item_id).val(), vendor_unit_price : $(".vendor_unit_price_"+item_id).val(), selling_price : $(".selling_price_"+item_id).val()},
          success: function (data) {
            if (data=="success") {
              updateAjax(item_id);
              $("#row_"+item_id).css('background-color', '#DFF0D8');
              $("#row_"+item_id).css('color', '#468847');
              $("#row_"+item_id).animate({
                backgroundColor: "FFFFFF",
                color: "#000",
                width: 500
                }, 1000 );
            }else{
              console.log("[ERROR]"+"\n"+data);
            };
          }
        });
    }

    function confirmDelete(item_id){
      $("#row_"+item_id).children(".actions").children(".edit").toggle();
      $("#row_"+item_id).children(".actions").children(".delete").toggle();
      $("#row_"+item_id).children(".actions").children(".confirmDelete").toggle();
      $("#row_"+item_id).children(".actions").children(".cancelDelete").toggle();
    }

    function deleteItem(item_id){
      $.ajax({
        url: '/items/'+item_id,
        type: 'delete',
        success: function (data) {
          if(data=="true"){
            $("#row_"+item_id).hide(function(){
              $("#row_"+item_id).remove()
              reindex()
            });
          }
        }
      });
    }

    function reindex(){
      index=1
      $("#payable_body > tr").each(function(){
        $(this).children(".no").text(index);
        index++
      })
    }
</script>
@stop
