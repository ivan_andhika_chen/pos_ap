<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableItems extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function(Blueprint $table) {
            $table->increments('id');
            $table->string('barcode');
            $table->string('name');
            $table->text('description');
            $table->integer('item_category_id');
            $table->integer('vendor_id');
            $table->integer('stock');
            $table->integer('real_stock');
            $table->decimal('vendor_unit_price',20,2);
            $table->decimal('selling_price',20,2);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items');
    }

}