<?php

class TransactionTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
    	DB::table('transactions')->delete();
        $date = new \DateTime;
        $transaction = array(
          'id'=>1,
          'number' =>'tr01',
          'date' =>$date,
          'employee_id' =>1,
          'customer_id' =>1,
          'paid_at' =>null,
          'grand_total' =>100000.00,
          'created_at'=> $date,
          'updated_at'=> $date
        );

        // Uncomment the below to run the seeder
        DB::table('transactions')->insert($transaction);
        $transaction = array(
          'id'=>2,
          'number' =>'tr02',
          'date' =>$date,
          'employee_id' =>1,
          'customer_id' =>1,
          'paid_at' =>null,
          'grand_total' =>200000.00,
          'created_at'=> $date,
          'updated_at'=> $date
        );

        // Uncomment the below to run the seeder
        DB::table('transactions')->insert($transaction);
    }

}