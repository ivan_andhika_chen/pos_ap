@if (count($errors->all()) > 0)
  <div class="alert alert-error fade in">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4>Error</h4>
    Please check the form below for errors
  </div>
@endif

@if ($message = Session::get('success'))
<div class="alert alert-success fade in">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>Success</h4>
    @if(is_array($message))
      @foreach ($message as $m)
        {{ $m }}
      @endforeach
    @else
      {{ $message }}
    @endif
  {{ Session::forget('success'); }}
</div>
@endif

@if ($message = Session::get('error'))
<div class="alert alert-error fade in">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>Error</h4>
    @if(is_array($message))
      @foreach ($message as $m)
        {{ $m }}
      @endforeach
    @else
      {{ $message }}
    @endif
  {{ Session::forget('error'); }}
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning fade in">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>Warning</h4>
    @if(is_array($message))
      @foreach ($message as $m)
        {{ $m }}
      @endforeach
    @else
      {{ $message }}
    @endif
  {{ Session::forget('warning'); }}
</div>
@endif

@if ($message = Session::get('info'))
<div class="alert alert-info fade in">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>Info</h4>
    @if(is_array($message))
      @foreach ($message as $m)
        {{ $m }}
      @endforeach
    @else
      {{ $message }}
    @endif
  {{ Session::forget('info'); }}
</div>
@endif