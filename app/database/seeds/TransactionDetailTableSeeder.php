<?php

class TransactionDetailTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
    	DB::table('transaction_details')->delete();
        $date = new \DateTime;
        $transaction_detail = array(
          'id' => '1',
          'transaction_id' => 1,
          'item_id' => 1,
          'quantity' => 2,
          'unit_price' => 50000.00,
          'total_price' => 100000.00,
          'deleted_at' => null,
          'created_at' => $date,
          'updated_at' => $date
        );

        DB::table('transaction_details')->insert($transaction_detail);
        $transaction_detail = array(
          'id' => '2',
          'transaction_id' => 2,
          'item_id' => 1,
          'quantity' => 1,
          'unit_price' => 50000.00,
          'total_price' => 50000.00,
          'deleted_at' => null,
          'created_at' => $date,
          'updated_at' => $date
        );

        DB::table('transaction_details')->insert($transaction_detail);
        $transaction_detail = array(
          'id' => '3',
          'transaction_id' => 2,
          'item_id' => 2,
          'quantity' => 3,
          'unit_price' => 50000.00,
          'total_price' => 150000.00,
          'deleted_at' => null,
          'created_at' => $date,
          'updated_at' => $date
        );

        DB::table('transaction_details')->insert($transaction_detail);
    }

}









