<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container-fluid">
      <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="brand" href="/">POS cloud</a>
      <div class="nav-collapse collapse">
        <ul class="nav">
          <li class="<?= Request::segment(1)=='dashboards' ? 'active': ''; ?>"><a href="/">Home</a></li>
          <li class="dropdown <?= Request::segment(1)=='items' ? 'active': ''; ?>">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Items<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/items">Items</a></li>
              <li><a href="/items/trash">Deleted Items</a></li>
              <!-- <li><a href="#">Stock Opname</a></li> -->
            </ul>
          </li>
          <li class="dropdown <?= Request::segment(1)=='transactions' ? 'active': ''; ?>" ><a href="/transactions/">Penjualan</a></li>
          <li class="dropdown <?= Request::segment(1)=='categories' ? 'active': ''; ?>" ><a href="/categories/">Category</a></li>
          <li class="dropdown <?= Request::segment(1)=='vendors' ? 'active': ''; ?>" ><a href="/vendors/">Vendor</a></li>
          <!-- <li class="dropdown <?= Request::segment(1)=='transactions' ? 'active': ''; ?>">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Penjualan<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/transactions/today">Transaksi Hari ini</a></li>
              <li><a href="/transactions/">Semua Transaksi</a></li>
            </ul>
          </li> -->
          <!-- <li class="dropdown <?= Request::segment(1)=='payables' ? 'active': ''; ?>">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Utang<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/payables">Daftar Utang</a></li>
            </ul>
          </li> -->
        </ul>
        <ul class="nav pull-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{Session::get('user')->name;}}<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <!-- <li><a href="#">Configuration</a></li> -->
              <li><a href="#">Feedback</a></li>
              <!-- <li><a href="#">Help</a></li> -->
              <li><a href="/logout">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </div>
</div>