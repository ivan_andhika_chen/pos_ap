<?php

class UserTypesTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
    	DB::table('user_types')->delete();
        $date = new \DateTime;
        $usertypes = array(
          'id'    => '1',
          'name'  => 'Admin',
          'created_at'=> $date,
          'updated_at'=> $date
        );

        // Uncomment the below to run the seeder
        DB::table('user_types')->insert($usertypes);

        $usertypes = array(
          'id'    => '2',
          'name'  => 'Cashier',
          'created_at'=> $date,
          'updated_at'=> $date
        );

        // Uncomment the below to run the seeder
        DB::table('user_types')->insert($usertypes);
    }

}