<?php

class Customer extends BaseModel {
  protected $guarded = array();
  public static $rules = array();

  public function transaction(){
    return $this->hasMany('Transaction');
  }
}