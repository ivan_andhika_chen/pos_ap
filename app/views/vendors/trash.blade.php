@extends('layouts.admin')
@section('style')
  <style type="text/css">
  </style>
@stop
@section('main')
  <div class="row-fluid">
    <div class="span9">
      <h3>List Deleted Vendors</h3>
    </div>
    <div class="span3">
      <h3 style="text-align:right;">
        &nbsp;<a id="trash" class="btn btn-mini btn-success" href="/vendors"><i class="icon-th-list icon-white"></i> Vendors</a>
      </h3>
    </div>
  </div>

  {{ Form::open(array('action' => 'VendorsController@store','class'=>'form-horizontal', 'id'=>'form_add_vendor', 'style'=> $errors=="[]" ? 'display:none':'' )) }}
  {{ Form::close() }}

  <table class="table table-striped table-bordered" style="background:white;">
    <thead>
      <tr>
        <th>No.</th>
        <th>Name</th>
        <th>Created</th>
        <th>Deleted</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody id="vendor_tbody">
      @if(count($vendors)>0)
        @foreach ($vendors as $key => $vendor)
          <?$key = $key+($vendors->count()*($vendors->getCurrentPage()-1));?>
          <tr id="row_{{($key+1)}}">
            <td class="id" style="display:none;">{{$vendor->id}}</td>
            <td class="no" style="width:50px;">{{($key+1+(($vendors->getCurrentPage()-1)*$vendors->getPerPage()))}}</td>
            <td class="editable name" >
              <span>{{$vendor->name; }}</span>
              {{ Form::text('name',$vendor->name,array('placeholder'=>'Fill name','class'=>'name_'.($key+1),'style'=>'display:none;'))}}
            </td>
            <td class="created" >
              <span>{{$vendor->created_at; }}</span>
            </td>
            <td class="deleted" >
              <span>{{$vendor->deleted_at; }}</span>
            </td>
            <td class="actions" style="width:180px;">
              {{ Form::button('<i class="icon-ok icon-white"></i> Restore', array('class'=>'restore btn btn-mini btn-success','onClick'=>"confirmRestore('".($key+1)."');")) }}
              {{ Form::button('<i class="icon-ok icon-white"></i> Restore', array('class'=>'confirm_restore btn btn-mini btn-success','style'=>'display:none;','onClick'=>"restoreCategory('".$vendor->id."','".($key+1)."');")) }}
              {{ Form::button('Cancel', array('class'=>'cancel_delete btn btn-mini','style'=>'display:none;','onClick'=>"confirmRestore('".($key+1)."');")) }}
            </td>
          </tr>
        @endforeach
      @else
        <tr class="info">
          <td colspan="6"><p class="text-center">No Data</p></td>
        </tr>
      @endif
    </tbody>
  </table>
  {{ $vendors->links() }}

@stop

@section('script')
  <script type="text/javascript">
    function confirmRestore(vendor_id){
      $("#row_"+vendor_id).children(".actions").children(".edit").toggle();
      $("#row_"+vendor_id).children(".actions").children(".restore").toggle();
      $("#row_"+vendor_id).children(".actions").children(".confirm_restore").toggle();
      $("#row_"+vendor_id).children(".actions").children(".cancel_delete").toggle();
    }

    function restoreCategory(vendor_id,row_id){
      $.ajax({
        url: '/vendors/restore',
        type: 'post',
        data: {'id':vendor_id},
        success: function (data) {
          if(data=="true"){
            $("#row_"+row_id).hide(function(){
              $("#row_"+row_id).remove()
              reindex()
            });
          }
        }
      });
    }

    function reindex(){
      index=1+{{($vendors->count()*($vendors->getCurrentPage()-1))}};
      $("#vendor_tbody > tr").each(function(){
        $(this).children(".no").text(index);
        index++
      })
    }
</script>
@stop