@extends('layouts.admin')
@section('main')
  {{ Form::open(array('url' => '/login','class'=> 'form-signin')) }}
    <h2 class="form-signin-heading">POScloud</h2>
    <h5 class="form-signin-heading">Admin Panel</h5>
    {{ Form::text('username',Input::old('username'),array('class'=>'input-block-level','placeholder'=>'Username')); }}
    {{ $errors->first('username') }}
    {{ Form::password('password',array('class'=>'input-block-level','placeholder'=>'Password')); }}
    {{ $errors->first('password') }}
    {{ Form::submit('Sign in',array('class'=>'btn btn-large btn-primary')); }}
  {{ Form::close() }}
@stop