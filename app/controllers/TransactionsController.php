<?php

class TransactionsController extends BaseController {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index($year=null,$month=null,$date = null){
    // $details = TransactionDetail::where('transaction_id','=',2)->first();
    // $s = $details->item()->first()->item_category()->withTrashed()->first()->name;
    // print_r($s);die();

    if($year==null){
      header("Location: /transactions/".date('Y/m/d',strtotime('now')));
      exit();
    }else{
      if ($month==null){
        if ($year==date('Y',strtotime('now'))){
          $date = date('d',strtotime('now'));
          $month = date('m',strtotime('now'));
        }else{
          $month="01";
          $date="01";
        }
        header("Location: /transactions/".$year."/".$month."/".$date);
        exit();
      }else{
        if ($date==null){
          header("Location: /transactions/".$year."/".$month."/01");
          exit();
        }
      }
    }
    
    $full_date = $year."-".$month."-".$date;
    $transactions = Transaction::
      where('date','like','%'.$full_date.'%')
      ->with('employee','customer','transaction_details','transaction_details.item')->paginate(10);

      // SELECT transaction_id,  sum((SELECT vendor_unit_price FROM items where items.id = item_id )*quantity), sum(total_price) FROM `transaction_details` GROUP BY transaction_id

    $group_months = TransactionDetail::statistic($month,$year)->get();

    // $queries = DB::getQueryLog();
    // $last_query = end($queries);
    // print_r($last_query);die();

    $ts = TransactionDetail::statistic($month,$year)
      ->where(DB::raw('DATE(transactions.date)'),'like','%'.$full_date.'%')
      ->get()->first();

    $modal = isset($ts) ? $ts->modal : 0;
    $total = isset($ts) ? $ts->omzet : 0;
    $data = array(
      'transactions' => $transactions, 
      'group_months' => $group_months,
      'date' => $full_date,
      'month' => $month,
      'year' => $year,
      'total'=> $total,
      'asset'=> $modal,
      'profit'=> $total-$modal
      );
    return View::make('transactions.index', $data);
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function today(){
    $transactions = Transaction::where('date','like','%'.date('Y-m-d',strtotime('now')).'%')->with('employee','customer')->paginate(10);
    return View::make('transactions.today', array('transactions' => $transactions));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    //
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $transaction = Transaction::find($id);
    if($transaction->transaction_details()->delete()){
      if($transaction->delete()){
        return "true";
      } 
    }else if($transaction->transaction_details()->count() <=0){
      if($transaction->delete()){
        return "true";
      } 
    }
    return "false";
  }

  public function import(){
    $date = new \DateTime;
    $file = Input::file('file');
    $path = $file->getRealPath();

    $objPHPExcel = PHPExcel_IOFactory::load($path);
    $t_id = array();
    $tid_index=0;
    $last_id=0;
    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
      $worksheetTitle = $worksheet->getTitle();
      $highestRow = $worksheet->getHighestRow(); // e.g. 10
      $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
      $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

      $nrColumns = ord($highestColumn) - 64;
      
      for ($row = 2; $row <= $highestRow; ++ $row) {
        $val=array();
        for ($col = 0; $col < $highestColumnIndex; ++ $col) {
          $cell = $worksheet->getCellByColumnAndRow($col, $row);
          $val[] = $cell->getValue();
        }
        $transaction = Transaction::where('number','=',$val[0])->get();
        if (count($transaction)==0){
          if($worksheetTitle == "Transaction"){
            //insert transaction
            $transaction_id = Transaction::insertGetId(
              array('number' => $val[0],
                    'date' => $val[1],
                    'employee_id' => $val[2],
                    'customer_id' => $val[3],
                    'paid_at' => $val[4],
                    'paid_total' => $val[5],
                    'grand_total' => $val[6],
                    'created_at'=> $date,
                    'updated_at'=> $date
                    )
            );
            array_push($t_id, $transaction_id);
          }
        } //34 35
        else if($worksheetTitle == "Transaction Detail"){
          //insert transaction details
          if ($last_id!=0){
            if ($last_id!=$val[0]){
              $tid_index++;
            }
          }
          $transaction_detail_id = TransactionDetail::insertGetId(
            array('transaction_id' => $t_id[$tid_index],
                  'item_id' => $val[1],
                  'quantity' => $val[2],
                  'unit_price' => $val[3],
                  'total_price' => $val[4],
                  'created_at'=> $date,
                  'updated_at'=> $date
                  )
          );
          $last_id=$val[0];
        }
      }
    }

    return Redirect::back()->with('success', 'Data Excel berhasil diinput');

  }

}