<?php

class Item extends BaseModel {
  protected $guarded = array();
  protected $softDelete = true;
  // relation

  public function item_category(){
    return $this->belongsTo('ItemCategory');
  }

  public function vendor(){
    return $this->belongsTo('Vendor');
  }

  public function transaction_details(){
    return $this->hasMany('TransactionDetail');
  }

  // rules

  public static $rules = array(
      'barcode' => 'required|numeric',
      'name'     => 'required|between:3,64',
      'item_category_id'  =>'required|numeric',
      'vendor_id'=>'required|numeric',
      'stock'=>'required|numeric',
      'real_stock'=>'required|numeric',
      'vendor_unit_price'=>'required|numeric',
      'selling_price'=>'required|numeric'
    );
}