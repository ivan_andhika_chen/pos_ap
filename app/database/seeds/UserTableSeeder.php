<?php

class UserTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
    	DB::table('users')->delete();
        $date = new \DateTime;
        $users = array(
          'id'    => '1',
          'name'  => 'Admin',
          'username' => 'administrator',
          'password'  => Hash::make('moshimoshi'),
          'user_type_id'=>1,
          'created_at'=> $date,
          'updated_at'=> $date
        );

        // Uncomment the below to run the seeder
        DB::table('users')->insert($users);

        $users = array(
          'id'    => '2',
          'name'  => 'Cashier',
          'username' => 'cashier',
          'password'  => Hash::make('123456'),
          'user_type_id'=>2,
          'created_at'=> $date,
          'updated_at'=> $date
        );

        // Uncomment the below to run the seeder
        DB::table('users')->insert($users);
    }

}