<?php

class VendorTableSeeder extends Seeder {

  public function run()
  {
    DB::table('vendors')->delete();
    $date = new \DateTime;
    $items = array(
      'id'=>'1',
      'name'=>'Axis',
      'contact_person' => 'John Budi',
      'address' => 'Purnawarman',
      'phone' => '02165025030',
      'created_at'=> $date,
      'updated_at'=> $date
    );
    // Uncomment the below to run the seeder
    DB::table('vendors')->insert($items);
    $items = array(
      'id'=>'2',
      'name'=>'Tri',
      'contact_person' => 'John Smith',
      'address' => 'Dago',
      'phone' => '0212500333',
      'created_at'=> $date,
      'updated_at'=> $date
    );
    // Uncomment the below to run the seeder
    DB::table('vendors')->insert($items);
  }

}