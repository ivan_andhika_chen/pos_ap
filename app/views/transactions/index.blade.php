@extends('layouts.admin')
@section('style')
  <style type="text/css">
    #payable_body tr td input,#payable_body tr td select{
      width: 100px;
      margin : 0px;
    }
    #payable_body tr td textarea{
      width: 150px;
      height: 100px;
    }
    #payable_body tr td.stock input,#payable_body tr td.real_stock input{
      width: 40px;
      margin : 0px;
    }
    .actions{
      width: 130px;
    }

  </style>
@stop
@section('main')
  <div class="row-fluid">
    <div class="span5">
      <h3>Daftar Transaksi <span style="color:#A60000;">{{date("d M Y",strtotime($date))}}</h3> </span>
    </div>
    <div class="span7 pull-right">
      {{ Form::open(array('url' => 'transactions/import', 'files' => true,'style="float:right;"')) }}
      {{ Form::file('file',array('id'=>'upload-file-field')); }}
      {{ Form::submit('Upload', array('class'=>'btn btn-primary','id'=>'button-upload')); }}
      {{ Form::close(); }}
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12 well well-small">
      <div class="span4"><b>Total:</b> <span class="money">{{$total}}</span></div>
      <div class="span4"><b>Modal:</b> <span class="money">{{$asset}}</span></div>
      <div class="span4"><b>Keuntungan:</b> <span class="money">{{$profit}}</span></div>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span2">
      <div id="datepicker"></div>
      <br>
      <table class="table table-striped table-bordered" style="background:white;">
        <thead>
          <tr>
            <th>Tgl - Modal - Omzet - Untung</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($group_months as $key => $sum)
            <tr>
              <td>
                <a href="/transactions/{{$year}}/{{$month}}/{{date("d",strtotime($sum->full_date))}}"><span style="color:#A60000;">{{date("d",strtotime($sum->date))}}</span>
                  <br/>Omzet: <span class="money">{{$sum->omzet}}</span>
                  <br/>Modal: <span class="money">{{$sum->modal}}</span>
                  <br/>Untung: <span class="money">{{$sum->omzet - $sum->modal}}</span>
                </a>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="span10">
      <table class="table table-striped table-bordered" style="background:white;">
        <thead>
          <tr>
            <th>No.</th>
            <th>Number</th>
            <th>Date</th>
            <th>Employee name</th>
            <th>Customer name</th>
            <th>Payment Date</th>
            <th>Grand Total </th>
            <th>Created</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody id="payable_body">
          @if(count($transactions)>0)
            @foreach ($transactions as $key => $transaction)
              <tr id="row_{{$transaction->id}}">
                <td class="no">{{($key+1+(($transactions->getCurrentPage()-1)*$transactions->getPerPage()))}}</td>
                <td class="number" >
                  <span>{{$transaction->number; }}</span>
                </td>
                <td class="date" >
                  <span>{{$transaction->date; }}</span>
                </td>
                <td class="employee_id" >
                  <span>{{$transaction->employee->name; }}</span>
                </td>
                <td class="customer_id" >
                  <span>{{$transaction->customer->name; }}</span>
                </td>
                <td class="payment_date" >
                  <span>{{$transaction->paid_at; }}</span>
                </td>
                <td class="grand_total right-text" >
                  <span class="money">{{$transaction->grand_total; }}</span>
                </td>
                <td>{{date("d-m-Y H:i",strtotime($transaction->created_at)); }}</td>
                <td class="actions">
                  {{ Form::button('<i class="icon-chevron-down icon-white"></i> Detail', array('id'=>'show_detail_'.$transaction->id,'class'=>'detail btn btn-mini btn-info','onClick'=>"showDetail('".$transaction->id."')")) }}
                  {{ Form::button('<i class="icon-chevron-up icon-white"></i> Hide', array('id'=>'hide_detail_'.$transaction->id,'class'=>'list-detail btn btn-mini btn-info','onClick'=>"hideDetail('".$transaction->id."')",'style'=>'display:none;')) }}
                  {{ Form::button('<i class="icon-trash icon-white"></i> Delete', array('id'=>'show_detail_'.$transaction->id,'class'=>'delete btn btn-mini btn-danger','onClick'=>"confirmDelete('".$transaction->id."')")) }}
                  {{ Form::button('<i class="icon-remove icon-white"></i> Delete', array('class'=>'confirmDelete btn btn-mini btn-danger','style'=>'display:none;','onClick'=>"deleteTransaction('".$transaction->id."');")) }}
                  {{ Form::button('Cancel', array('class'=>'cancelDelete btn btn-mini','style'=>'display:none;','onClick'=>"confirmDelete('".$transaction->id."');")) }}
                </td>
              </tr>
              <tr class="detail" id="detail_{{$transaction->id}}" style="display:none;">
                <?php $details = TransactionDetail::where('transaction_id','=',$transaction->id)->get();?>
                <td colspan="9">
                  <table class="table table-striped table-bordered">
                    <thead>
                      <tr style="background-color:#D9EDF7;">
                        <th>Barcode</th>
                        <th>Item Name</th>
                        <th>Category</th>
                        <th>Vendor</th>
                        <th>Qty</th>
                        <th>Unit Price</th>
                        <th>Vendor Unit Price</th>
                        <th>Subtotal</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($details as $key => $detail)
                        <tr>
                          <td>{{$detail->item()->withTrashed()->first()->barcode}}</td>
                          <td>{{$detail->item()->withTrashed()->first()->name}}</td>
                          <td>{{$detail->item()->withTrashed()->first()->item_category()->withTrashed()->first()->name}}</td>
                          <td>{{$detail->item()->withTrashed()->first()->vendor()->withTrashed()->first()->name}}</td>
                          <td>{{$detail->quantity}}</td>
                          <td class="money">{{$detail->unit_price}}</td>
                          <td class="money">{{$detail->item()->withTrashed()->first()->vendor_unit_price}}</td>
                          <td class="money">{{$detail->total_price}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </td>
              </tr>
            @endforeach
          @else
            <tr class="info">
              <td colspan="9"><p class="text-center">No Data</p></td>
            </tr>
          @endif
        </tbody>
      </table>
      {{ $transactions->links() }}
      </div>
  </div>
@stop

@section('script')
  <script type="text/javascript">
    $(document).ready(function(){
      $( "#datepicker" ).datepicker({
        dateFormat: "yy/mm/dd",
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onSelect: function(){ 
          window.location.replace("/transactions/"+$( "#datepicker" ).val());
        }
      });
      $( "#datepicker" ).datepicker("setDate","{{Request::segment(2)}}/{{Request::segment(3)}}/{{Request::segment(4)}}")
      $(".ui-datepicker-current").click(function(){
        window.location.replace("/transactions/"+$.datepicker.formatDate('yy/mm/dd', new Date()));
      })
      $(".ui-datepicker-current").click(function(){
      });

      $("#button-upload").click(function(){
        if ($("#upload-file-field").val()=="") {
          alert("Pilih file terlebih dahulu.");
          return false;
        };
      })
    })

    function createNew(){
      $('#form_add_item').slideDown();
      $('#add_item').slideUp();
    }

    function cancelNew(){
      $('#form_add_item').slideUp();
      $('#add_item').slideDown();
    }

    $(".editable").click(function(){
      $(this).children("input").show();
      $(this).children("textarea").show();
      $(this).children("select").show();
      $(this).children("span").hide();
      $(this).siblings(".actions").children('.edit').hide();
      $(this).siblings(".actions").children('.delete').hide();
      $(this).siblings(".actions").children('.update').show();
      $(this).siblings(".actions").children('.cancel_update').show();
    })

    function cancelUpdate(row_id){
      $("#row_"+row_id+" > td").each(function(){
        $(this).children("span").show();
        $(this).children("input").hide();
        $(this).children("textarea").hide();
        $(this).children("select").hide();
      })
        $("#row_"+row_id).children(".actions").children('.edit').show();
        $("#row_"+row_id).children(".actions").children('.delete').show();
        $("#row_"+row_id).children(".actions").children('.update').hide();
        $("#row_"+row_id).children(".actions").children('.cancel_update').hide();
    }

    function updateAjax(row_id){
      $("#row_"+row_id+" > td").each(function(){
        if ($(this).children("input").length >0){
          $(this).children("span").text($(this).children("input").val());
        }
        if ($(this).children("textarea").length >0){
          $(this).children("span").text($(this).children("textarea").val());
        }
        if ($(this).children("select").length >0){
          $(this).children("span").text($(this).children("select").text());
        }
        $(this).children("span").show();
        $(this).children("input").hide();
        $(this).children("textarea").hide();
        $(this).children("select").hide();
      })
      $("#row_"+row_id).children(".actions").children('.edit').show();
      $("#row_"+row_id).children(".actions").children('.delete').show();
      $("#row_"+row_id).children(".actions").children('.update').hide();
      $("#row_"+row_id).children(".actions").children('.cancel_update').hide();
    }

    function showDetail(payable_id){
      $("#detail_"+payable_id).show();
      $('#show_detail_'+payable_id).hide();
      $('#hide_detail_'+payable_id).show();
    }

    function hideDetail(payable_id){
      $("#detail_"+payable_id).hide();
      $('#show_detail_'+payable_id).show();
      $('#hide_detail_'+payable_id).hide();
    }

    function update(item_id){
      $.ajax({
          url: '/items/'+item_id,
          type: 'put',
          data: {_token: "{{csrf_token()}}", id : item_id, barcode : $(".barcode_"+item_id).val(), name : $(".name_"+item_id).val(), description : $(".description_"+item_id).val(), item_category_id : $(".item_category_"+item_id).val(), vendor_id : $(".vendor_"+item_id).val(), stock : $(".stock_"+item_id).val(), real_stock : $(".real_stock_"+item_id).val(), vendor_unit_price : $(".vendor_unit_price_"+item_id).val(), selling_price : $(".selling_price_"+item_id).val()},
          success: function (data) {
            if (data=="success") {
              updateAjax(item_id);
              $("#row_"+item_id).css('background-color', '#DFF0D8');
              $("#row_"+item_id).css('color', '#468847');
              $("#row_"+item_id).animate({
                backgroundColor: "FFFFFF",
                color: "#000",
                width: 500
                }, 1000 );
            }else{
              console.log("[ERROR]"+"\n"+data);
            };
          }
        });
    }

    function confirmDelete(item_id){
      if ($("#detail_"+item_id).is(':visible')){
        $("#row_"+item_id).children(".actions").children(".list-detail").toggle();
      }else {
        $("#row_"+item_id).children(".actions").children(".detail").toggle();
      }
      $("#row_"+item_id).children(".actions").children(".delete").toggle();
      $("#row_"+item_id).children(".actions").children(".confirmDelete").toggle();
      $("#row_"+item_id).children(".actions").children(".cancelDelete").toggle();
    }

    function deleteTransaction(item_id){
      $.ajax({
        url: '/transactions/'+item_id,
        type: 'delete',
        success: function (data) {
          if(data=="true"){
            $("#row_"+item_id).hide(function(){
              $("#row_"+item_id).remove()
              reindex()
            });
          }
        }
      });
    }

    function reindex(){
      index=1
      $("#payable_body > tr").each(function(){
        $(this).children(".no").text(index);
        index++
      })
    }
</script>
@stop
