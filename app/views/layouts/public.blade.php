bootstrap<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="/assets/css/public.css" rel="stylesheet" type="text/css">
    <title>Fancy Kosmetik</title>
  </head>

  <body>
    <!--[if gte IE 9]>
      <style type="text/css">
        .gradient {
           filter: none;
        }
      </style>
    <![endif]-->
    @if (Session::has('message'))
      <div class="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Warning!</strong> {{ Session::get('message') }}
      </div>
    @endif

    <div id="wrap">
      <!-- Fixed navbar -->
        @include('layouts.header')
      <!-- end Fixed navbar -->
      <div class="container">
        @yield('main')
      </div>
      <!-- <div id="push"></div> -->
    </div>

    <!-- sticky footer -->
      <!--@include('layouts.footer')-->
    <!-- end sticky footer -->
    <script type="text/javascript" src="/assets/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>