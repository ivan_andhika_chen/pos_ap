<?php

class TransactionDetail extends BaseModel {
  protected $guarded = array();
  public static $rules = array();
  protected $softDelete = true;
  public function trancation(){
    return $this->belongsTo('Transaction');
  }
  public function item(){
    return $this->belongsTo('Item')->with('item_category','vendor');
  }

  public function scopeStatistic($query,$month,$year){
    return $query->select(DB::raw('transactions.date as full_date, DATE(transactions.date) as date, sum(vendor_unit_price*quantity) as modal, sum(total_price) as omzet'))
      ->join('items', 'items.id', '=', 'item_id')
      ->join('transactions', 'transactions.id', '=', 'transaction_id')
      ->where(DB::raw('MONTH(transactions.date)'),"=",$month)
      ->where(DB::raw('YEAR(transactions.date)'),"=",$year)
      ->groupBy(DB::raw('DATE(transactions.date)'));
  }
}