<?php

class Vendor extends BaseModel {
  protected $guarded = array();
  protected $softDelete = true;
  public static $rules = array();
  public function items(){
    return $this->hasMany('Item');
  }
}