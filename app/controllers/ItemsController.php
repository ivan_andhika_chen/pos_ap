<?php

class ItemsController extends BaseController {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index(){
    $items = Item::with('item_category','vendor')->paginate(10);
    $vendor_options = Vendor::lists('name', 'id');
    $item_category_options = ItemCategory::lists('name', 'id');
    return View::make('items.index', array('items' => $items,'item_category_options'=>$item_category_options,'vendor_options'=>$vendor_options));
  }

  /**
   * Show the deleted items
   *
   * @return Response
   */
  public function trash(){
    $items = Item::onlyTrashed()->with('item_category','vendor')->paginate(10);
    return View::make('items.trash', array('items' => $items));
  }
  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(){
    Item::$rules['barcode'] = Item::$rules['barcode'] . '|unique:items';
    $item = new Item;
    $item->barcode = Input::get('new_barcode');
    $item->name = Input::get('new_name');
    $item->description = Input::get('new_description');
    $item->stock = Input::get('new_stock');
    $item->real_stock = Input::get('new_real_stock');
    $item->unit = Input::get('new_unit');
    $item->vendor_unit_price = Input::get('new_vendor_unit_price');
    $item->selling_price = Input::get('new_selling_price');

    $param_vendor = Input::get('new_vendor');
    $param_item_category = Input::get('new_item_category');
    if (!empty($param_vendor)){
      $vendor = new Vendor;
      $vendor->name = $param_vendor;
      $vendor->save();
      $item->vendor_id = $vendor->id;
    }else{
      $item->vendor_id = Input::get('new_vendor_id');
    }
    if (!empty($param_item_category)){
      $item_category = new ItemCategory;
      $item_category->name = $param_item_category;
      $item_category->save();
      $item->item_category_id = $item_category->id;
    }else{
      $item->item_category_id = Input::get('new_item_category_id');
    }
    if ($item->save()) return Redirect::route('items.index');
    return Redirect::back()->withInput()->withErrors($item->errors);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id){
    $post = new Item(Input::all());
    $item = Item::find($post->id);
    $item->barcode = $post->barcode;
    $item->name = $post->name;
    $item->description = $post->description;
    $item->item_category_id = $post->item_category_id;
    $item->vendor_id = $post->vendor_id;
    $item->stock = $post->stock;
    $item->real_stock = $post->real_stock;
    $item->unit = $post->unit;
    $item->vendor_unit_price = $post->vendor_unit_price;
    $item->selling_price = $post->selling_price;
    if ($item->save()) return "success";
    // return Redirect::back()->withInput()->withErrors($item->errors);
    return serialize(json_decode($item->errors));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $item = Item::find($id);
    if($item->delete()) return "true";
    return "false";
  }

  public function restore(){
    $id = Input::get('id');
    if(empty($id)) return "false";
    $item = Item::onlyTrashed()->find($id);
    if($item->restore()) return "true";
    return "false";
  }

  public function import(){
    $timezone = "Asia/Bangkok";
    date_default_timezone_set($timezone);
    $date = new \DateTime;

    if (Input::hasFile('file')){
      $file = Input::file('file');
      $path = $file->getRealPath();
      $mime = $file->getMimeType();
      $mimeTypes = ["application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-office","application/msword"];

      if(in_array($mime, $mimeTypes)){
        $objPHPExcel = PHPExcel_IOFactory::load($path);
        $succeed_id = array();
        $rejected_id = array();
        $tid_index=0;
        $last_id=0;
        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
          $worksheetTitle = $worksheet->getTitle();
          $highestRow = $worksheet->getHighestRow(); // e.g. 10
          $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
          $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
          $nrColumns = ord($highestColumn) - 64;
          
          for ($row = 2; $row <= $highestRow; ++ $row) {
            $val=array();
            for ($col = 0; $col < $highestColumnIndex; ++ $col) {
              $cell = $worksheet->getCellByColumnAndRow($col, $row);
              $val[] = $cell->getValue();
            }

            if(!empty($val[0]) && !empty($val[1]) && !empty($val[3]) && !empty($val[4]) && !empty($val[5]) && !empty($val[6]) && !empty($val[7]) && !empty($val[8]) && !empty($val[9])){
              $item = Item::where('barcode', '=', $val[0])->first();
              $category = ItemCategory::where('name', 'like', $val[3])->first();
              $vendor = Vendor::where('name', 'like', $val[4])->first();

              if(empty($category)){
                $category_id = ItemCategory::insertGetId(
                array('name' => $val[3],
                      'created_at'=> $date,
                      'updated_at'=> $date
                     )
                );
              }else{
                $category_id = $category->id;
              }

              if(empty($vendor)){
                $vendor_id = Vendor::insertGetId(
                array('name' => $val[4],
                      'created_at'=> $date,
                      'updated_at'=> $date
                     )
                );
              }else{
                $vendor_id = $vendor->id;
              }

              if(empty($item)){
                //insert item
                $item_id = Item::insertGetId(
                array('barcode' => $val[0],
                      'name' => $val[1],
                      'description' => $val[2],
                      'item_category_id' => $category_id,
                      'vendor_id' => $vendor_id,
                      'stock' => $val[5],
                      'real_stock' => $val[6],
                      'unit' => $val[7],
                      'vendor_unit_price' => $val[8],
                      'selling_price' => $val[9],
                      'created_at'=> $date,
                      'updated_at'=> $date
                     )
                );
                array_push($succeed_id, $val[0]);
              }else{
                array_push($rejected_id, $val[0]);
              }
            }
            else{
              array_push($rejected_id, $val[0]);
            }
          }
        }
      }else{
        return Redirect::back()->with('error', "File yang anda masukkan salah.");
      }

      $messages = array();
      if(!empty($succeed_id)){
        $message = 'Data Item dengan barcode '.join(', ', $succeed_id).' berhasil dimasukkan.';
        array_push($messages, $message);
      }
      if(!empty($rejected_id)){
        $message = 'Data Item dengan barcode '.join(', ', $rejected_id).' gagal dimasukkan, mohon periksa kembali data item tersebut.';
        array_push($messages, $message);
      }
    }else{
      return Redirect::back()->with('error', "Tidak ada file yang anda masukkan.");
    }  
    
    if(!empty($rejected_id) ){
      return Redirect::back()->with('error', $messages);
    }else{
      return Redirect::back()->with('success', $messages);
    }
    
  }
}