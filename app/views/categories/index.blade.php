@extends('layouts.admin')
@section('style')
  <style type="text/css">
  </style>
@stop
@section('main')
  <div class="row-fluid">
    <div class="span9">
      <h3>List Categories &nbsp;<button id="add_item" class="btn btn-mini btn-primary" type="button" onClick="createNew();" style="{{ $errors!='[]' ? 'display:none':'' }}"><i class="icon-pencil icon-white"></i> Add</button></h3>
    </div>
    <div class="span3">
      <h3 style="text-align:right;">
        &nbsp;<a id="trash" class="btn btn-mini btn-danger" href="categories/trash"><i class="icon-trash icon-white"></i> Trash</a>
      </h3>
    </div>
  </div>

  {{ Form::open(array('action' => 'CategoriesController@store','class'=>'form-horizontal', 'id'=>'form_add_category', 'style'=> $errors=="[]" ? 'display:none':'' )) }}
  {{ Form::close() }}

  <table class="table table-striped table-bordered" style="background:white;">
    <thead>
      <tr>
        <th>No.</th>
        <th>Name</th>
        <th>Created</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody id="category_tbody">
      @if(count($categories)>0)
        @foreach ($categories as $key => $category)
          <tr id="row_{{($key+1)}}">
            <td class="id" style="display:none;">{{$category->id}}</td>
            <td class="no" style="width:50px;">{{($key+1+(($categories->getCurrentPage()-1)*$categories->getPerPage()))}}</td>
            <td class="editable name" >
              <span>{{$category->name; }}</span>
              {{ Form::text('name',$category->name,array('placeholder'=>'Fill name','class'=>'name_'.($key+1),'style'=>'display:none;'))}}
            </td>
            <td class="created" >
              <span>{{$category->created_at; }}</span>
            </td>
            <td class="actions" style="width:180px;">
              {{ Form::button('<i class="icon-edit icon-white"></i> Edit', array('class'=>'edit btn btn-mini btn-primary')) }}
              {{ Form::button('<i class="icon-remove icon-white"></i> Delete', array('class'=>'delete btn btn-mini btn-danger','onClick'=>"confirmDelete('".($key+1)."','".$category->id."');")) }}

              {{ Form::button('<i class="icon-ok icon-white"></i> Update', array('class'=>'update btn btn-mini btn-success','style'=>'display:none;','onClick'=>"update('".($key+1)."','".$category->id."')")) }}
              {{ Form::button('Cancel', array('class'=>'cancel_update btn btn-mini','style'=>'display:none;','onClick'=>"cancelUpdate('".($key+1)."','".$category->id."');")) }}

              {{ Form::button('<i class="icon-remove icon-white"></i> Delete', array('class'=>'confirmDelete btn btn-mini btn-danger','style'=>'display:none;','onClick'=>"deleteCategory('".($key+1)."','".$category->id."');")) }}
              {{ Form::button('Cancel', array('class'=>'cancelDelete btn btn-mini','style'=>'display:none;','onClick'=>"confirmDelete('".($key+1)."','".$category->id."');")) }}
            </td>
          </tr>
        @endforeach
      @else
        <tr class="info">
          <td colspan="6"><p class="text-center">No Data</p></td>
        </tr>
      @endif
    </tbody>
  </table>
  {{ $categories->links() }}

  <div style="display:none;">
    <table>
      <tbody id="abstract_new">
        <tr>
          <td class="id" style="display:none;"></td>
          <td class="no" style="width:50px;"></td>
          <td class="editable name" >
            <span></span>
            {{ Form::text('name',null,array('placeholder'=>'Fill name','class'=>'name_'))}}
          </td>
          <td class="created" >
          </td>
          <td class="actions" style="width:180px;">
            {{ Form::button('<i class="icon-ok icon-white"></i> Save', array('class'=>'save btn btn-mini btn-success','onClick'=>"save('')")) }}
            {{ Form::button('<i class="icon-edit icon-white"></i> Edit', array('class'=>'edit btn btn-mini btn-primary', 'style'=>'display:none;')) }}
            {{ Form::button('<i class="icon-ok icon-white"></i> Update', array('class'=>'update btn btn-mini btn-success','style'=>'display:none;','onClick'=>"update('','')")) }}
            {{ Form::button('Cancel', array('class'=>'cancel_update btn btn-mini','style'=>'display:none;','onClick'=>"cancelUpdate('','');")) }}

            {{ Form::button('<i class="icon-remove icon-white"></i> Delete', array('class'=>'delete btn btn-mini btn-danger','onClick'=>"confirmDelete('');")) }}

            {{ Form::button('<i class="icon-remove icon-white"></i> Delete', array('class'=>'confirmDelete btn btn-mini btn-danger','style'=>'display:none;','onClick'=>"deleteCategory('');")) }}
            {{ Form::button('Cancel', array('class'=>'cancelDelete btn btn-mini','style'=>'display:none;','onClick'=>"confirmDelete('');")) }}
          </td>
        </tr>
      </tbody>
    </table>
  </div>
@stop

@section('script')
  <script type="text/javascript">
    $(document).ready(function(){
      numberOnly(".number")
      numberOnly(".decimal")
      @if (Input::old('item_category'))
        $(".choose_item_category").hide();
        $(".new_item_category").show();
      @else
        $(".choose_item_category").show();
        $(".new_item_category").hide();
      @endif

      @if (Input::old('vendor'))
        $(".choose_vendor").hide();
        $(".new_vendor").show();
      @else
        $(".choose_vendor").show();
        $(".new_vendor").hide();
      @endif

      editable();
    })

    function createNew(){
      $("#category_tbody").append($("#abstract_new").html());
      reindex()
    }

    function editable(){
      $(".editable").click(function(){
        $(this).children("input").show();
        $(this).children("textarea").show();
        $(this).children("select").show();
        $(this).children("span").hide();
        $(this).siblings(".actions").children('.edit').hide();
        $(this).siblings(".actions").children('.delete').hide();
        $(this).siblings(".actions").children('.update').show();
        $(this).siblings(".actions").children('.cancel_update').show();
      })
      $(".edit").click(function(){
        $(this).parent().siblings(".name").children("span").hide();

        $(this).parent().siblings(".name").children("input").show();

        $(this).parent().children('.edit').hide();
        $(this).parent().children('.save').hide();
        $(this).parent().children('.delete').hide();
        $(this).parent().children('.update').show();
        $(this).parent().children('.cancel_update').show();

      })
    }


    function cancelUpdate(row_id,id){
      $("#row_"+row_id+" > td").each(function(){
        $(this).children("span").show();
        $(this).children("input").hide();
        $(this).children("textarea").hide();
        $(this).children("select").hide();
      })
        $("#row_"+row_id).children(".actions").children('.edit').show();
        $("#row_"+row_id).children(".actions").children('.delete').show();
        $("#row_"+row_id).children(".actions").children('.update').hide();
        $("#row_"+row_id).children(".actions").children('.cancel_update').hide();
    }

    function updateAjax(row_id,id){
      $("#row_"+row_id+" > td").each(function(){
        if ($(this).children("input").length >0){
          $(this).children("span").text($(this).children("input").val());
        }
        if ($(this).children("textarea").length >0){
          $(this).children("span").text($(this).children("textarea").val());
        }
        if ($(this).children("select").length >0){
          $(this).children("span").text($(this).children("select").find(":selected").text());
        }
        $(this).children("span").show();
        $(this).children("input").hide();
        $(this).children("textarea").hide();
        $(this).children("select").hide();
      })
      $("#row_"+row_id).children(".actions").children('.save').hide();
      $("#row_"+row_id).children(".actions").children('.edit').show();
      $("#row_"+row_id).children(".actions").children('.delete').show();
      $("#row_"+row_id).children(".actions").children('.update').hide();
      $("#row_"+row_id).children(".actions").children('.cancel_update').hide();
    }

    function update(category_id,id){
      $.ajax({
          url: '/categories/'+id,
          type: 'put',
          data: {_token: "{{csrf_token()}}", id : id, name : $(".name_"+category_id).val()},
          success: function (data) {
            if (data=="success") {
              updateAjax(category_id,id);
              $("#row_"+category_id).css('background-color', '#DFF0D8');
              $("#row_"+category_id).css('color', '#468847');
              $("#row_"+category_id).animate({
                backgroundColor: "FFFFFF",
                color: "#000",
                width: 500
                }, 1000 );
              toCurrency(['.money'])
            }else{
              console.log("[ERROR]"+"\n"+data);
            };
          }
        });
    }

    function save(index,id){
      $.ajax({
          url: '/categories',
          type: 'post',
          data: {_token: "{{csrf_token()}}", id : id, name : $(".name_"+index).val()},
          success: function (data) {
            data = jQuery.parseJSON(data)
            id= data.id
            if (data.id!="") {
              $("#row_"+index).children(".id").text( data.id )
              $("#row_"+index).children(".name").children("span").text( data.name )
              $("#row_"+index).children(".created").text( data.created_at )

              $("#row_"+index).children(".actions").children(".update").attr("onClick","update('"+index+"','"+id+"')");
              $("#row_"+index).children(".actions").children(".cancel_update").attr("onClick","deleteCategory('"+index+"','"+id+"')");
              $("#row_"+index).children(".actions").children(".delete").attr("onClick","confirmDelete('"+index+"','"+id+"')");
              $("#row_"+index).children(".actions").children(".save").attr("onClick","save('"+index+"','"+id+"')");
              $("#row_"+index).children(".actions").children(".confirmDelete").attr("onClick","deleteCategory('"+index+"','"+id+"')");
              $("#row_"+index).children(".actions").children(".cancelDelete").attr("onClick","confirmDelete('"+index+"','"+id+"')");

              updateAjax(index,id);
              $("#row_"+index).css('background-color', '#DFF0D8');
              $("#row_"+index).css('color', '#468847');
              $("#row_"+index).animate({
                backgroundColor: "FFFFFF",
                color: "#000",
                width: 500
                }, 1000 );
            }else{
              console.log("[ERROR]"+"\n"+data);
            };
            editable()
          }
        });
    }

    function confirmDelete(category_id,id){
      $("#row_"+category_id).children(".actions").children(".delete").toggle();
      $("#row_"+category_id).children(".actions").children(".confirmDelete").toggle();
      $("#row_"+category_id).children(".actions").children(".cancelDelete").toggle();
    }

    function deleteCategory(category_id,id){
      if(!id!=""){
        $("#row_"+category_id).hide(function(){
          $("#row_"+category_id).remove()
          reindex()
        });
      }else{
        $.ajax({
          url: '/categories/'+id,
          type: 'delete',
          success: function (data) {
            if(data=="true"){
              $("#row_"+category_id).hide(function(){
                $("#row_"+category_id).remove()
                reindex()
              });
            }
          }
        });
      }
    }

    function reindex(){
      index=1
      $("#category_tbody > tr").each(function(){
        $(this).children(".no").text(index);
        id = $(this).children(".id").text();
        $(this).show();
        $(this).attr("id","row_"+index);
        $(this).children(".name").children("input").attr('class','name_'+index);
        $(this).children(".actions").children(".delete").attr("onClick","confirmDelete('"+index+"','"+id+"')");
        $(this).children(".actions").children(".save").attr("onClick","save('"+index+"','"+id+"')");
        $(this).children(".actions").children(".confirmDelete").attr("onClick","deleteCategory('"+index+"','"+id+"')");
        $(this).children(".actions").children(".cancelDelete").attr("onClick","confirmDelete('"+index+"','"+id+"')");
        index++
      })
    }

</script>
@stop
