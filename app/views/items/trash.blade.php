@extends('layouts.admin')
@section('main')
  <div class="row-fluid">
    <div class="span9">
      <h3>List Deleted Item</h3>
    </div>
    <div class="span3">
      <h3 style="text-align:right;">
        &nbsp;<a id="trash" class="btn btn-mini btn-success" href="/items"><i class="icon-th-list icon-white"></i> Item</a>
      </h3>
    </div>
  </div>
  <table class="table table-striped table-bordered" style="background:white;">
    <thead>
      <tr>
        <th>No.</th>
        <th>Barcode</th>
        <th>Name</th>
        <th>Description</th>
        <th>Item Category</th>
        <th>Vendor</th>
        <th>Stock</th>
        <th>Real Stock</th>
        <th>Unit</th>
        <th>Vendor Unit Price</th>
        <th>Selling Price</th>
        <th>Created</th>
        <th>Deleted</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody id="item_body">
      @if(count($items)>0)
        @foreach ($items as $key => $item)
          <tr id="row_{{$item->id}}">
            <td class="no">{{($key+1) }}</td>
            <td>{{$item->barcode; }}</td>
            <td>{{$item->name; }}</td>
            <td>{{$item->description; }}</td>
            <td>{{$item->item_category->name; }}</td>
            <td>{{$item->vendor->name; }}</td>
            <td>{{$item->stock; }}</td>
            <td>{{$item->real_stock; }}</td>
            <td>{{$item->unit; }}</td>
            <td class="money">{{$item->vendor_unit_price; }}</td>
            <td class="money">{{$item->selling_price; }}</td>
            <td>{{date("d-m-Y",strtotime($item->created_at)); }}</td>
            <td>{{date("d-m-Y",strtotime($item->deleted_at)); }}</td>
            <td class="actions">
              {{ Form::button('<i class="icon-ok icon-white"></i> Restore', array('class'=>'restore btn btn-mini btn-success','onClick'=>"confirm_restore('".$item->id."');")) }}
              {{ Form::button('<i class="icon-ok icon-white"></i> Restore', array('class'=>'confirm_restore btn btn-mini btn-success','style'=>'display:none;','onClick'=>"restore_item('".$item->id."');")) }}
              {{ Form::button('Cancel', array('class'=>'cancel_delete btn btn-mini','style'=>'display:none;','onClick'=>"confirm_restore('".$item->id."');")) }}
            </td>
          </tr>
        @endforeach
      @else
        <tr class="info">
          <td colspan="13"><p class="text-center">No Data</p></td>
        </tr>
      @endif
    </tbody>
  </table>
  {{ $items->links() }}
@stop
@section('script')
  <script type="text/javascript">
    function confirm_restore(item_id){
      $("#row_"+item_id).children(".actions").children(".edit").toggle();
      $("#row_"+item_id).children(".actions").children(".restore").toggle();
      $("#row_"+item_id).children(".actions").children(".confirm_restore").toggle();
      $("#row_"+item_id).children(".actions").children(".cancel_delete").toggle();
    }

    function restore_item(item_id){
      $.ajax({
        url: '/items/restore',
        type: 'post',
        data: {'id':item_id},
        success: function (data) {
          if(data=="true"){
            $("#row_"+item_id).hide(function(){
              $("#row_"+item_id).remove()
              reindex()
            });
          }
        }
      });
    }

    function reindex(){
      index=1
      $("#item_body > tr").each(function(){
        $(this).children(".no").text(index);
        index++
      })
    }
</script>
@stop
