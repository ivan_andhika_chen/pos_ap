<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
  return Redirect::to('/dashboards');
});

Route::get('login', function() {
    if(Auth::check()){
      return Redirect::to('/dashboards');
    }
    return View::make('auth.login');
  });

  Route::post('login', function() {
    // get POST data
    $userdata = array(
      'username' => Input::get('username'),
      'password' => Input::get('password')
    );

    $user = User::where('username', '=',Input::get('username'))->first();
    if ($user->user_type_id==1){
      if ( Auth::attempt($userdata) ){
        // we are now logged in, go to home
        Session::put('user', $user);
        return Redirect::to('dashboards')->with('success', "Selamat datang, $user->name" );
      }else{
        // auth failure! lets go back to the login
        return Redirect::to('login')
        ->with('error', 'Username atau password Anda salah');
        // pass any error notification you want
        // i like to do it this way :)
      }
    }else{
      return Redirect::to('login')->with('error', "Anda Tidak memiliki hak akses" );
    }
  });

  Route::get('logout', function() {
    Auth::logout();
    return Redirect::to('login');
  });

Route::group(array('before' => 'auth'), function()
{
  Route::resource('dashboards', 'DashboardsController');
  
  Route::get('items/trash', array('uses' => 'ItemsController@trash'));
  Route::post('items/restore', array('uses' => 'ItemsController@restore'));
  Route::post('items', array('before' => 'csrf', 'uses' => 'ItemsController@store'));
  Route::put('items', array('before' => 'csrf', 'uses' => 'ItemsController@update'));
  Route::delete('items', array('before' => 'csrf', 'uses' => 'ItemsController@destroy'));
  Route::resource('items', 'ItemsController');
  Route::post('items/import', array('uses' => 'ItemsController@import'));

  Route::get('payables/paid', array('uses' => 'PayablesController@paid'));
  Route::resource('payables', 'PayablesController');

  Route::get('transactions/today', array('uses' => 'TransactionsController@today'));
  Route::get('transactions/{year}', array('uses' => 'TransactionsController@index'));
  Route::get('transactions/{year}/{month}', array('uses' => 'TransactionsController@index'));
  Route::get('transactions/{year}/{month}/{date}', array('uses' => 'TransactionsController@index'));
  Route::post('transactions/import', array('uses' => 'TransactionsController@import'));
  Route::resource('transactions', 'TransactionsController');

  Route::get('categories/trash', array('uses' => 'CategoriesController@trash'));
  Route::post('categories/restore', array('uses' => 'CategoriesController@restore'));
  Route::post('categories', array('before' => 'csrf', 'uses' => 'CategoriesController@store'));
  Route::put('categories', array('before' => 'csrf', 'uses' => 'CategoriesController@update'));
  Route::delete('categories', array('before' => 'csrf', 'uses' => 'CategoriesController@destroy'));
  Route::resource('categories', 'CategoriesController');
  Route::get('vendors/trash', array('uses' => 'VendorsController@trash'));
  Route::post('vendors/restore', array('uses' => 'VendorsController@restore'));
  Route::post('vendors', array('before' => 'csrf', 'uses' => 'VendorsController@store'));
  Route::put('vendors', array('before' => 'csrf', 'uses' => 'VendorsController@update'));
  Route::delete('vendors', array('before' => 'csrf', 'uses' => 'VendorsController@destroy'));
  Route::resource('vendors', 'VendorsController');
});
