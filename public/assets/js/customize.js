$(document).ready(function(){
  numberOnly(".number")
  numberOnly(".decimal")
  toCurrency(['.money'])
})

function numberOnly(field){
  $(field).keydown(function(e) {
    return ( (e.keyCode >= 48 && e.keyCode <= 57) || e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 9 || e.keyCode == 16 || e.keyCode == 116 || (e.keyCode >= 96 && e.keyCode <= 105))
  });
}