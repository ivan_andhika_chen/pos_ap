<?php

class VendorsController extends BaseController {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $data = array(
    'vendors' => Vendor::paginate(10)
      );
    return View::make("vendors.index", $data);
  }

  public function trash()
  {
    $data = array(
    'vendors' => Vendor::onlyTrashed()->paginate(10)
      );
    return View::make("vendors.trash", $data);
  }

  public function restore()
  {
    $id = Input::get('id');
    if(empty($id)) return "false";
    $vendor = Vendor::onlyTrashed()->find($id);
    if($vendor->restore()) return "true";
    return "false";
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    $vendor = new Vendor;
    $vendor->name = Input::get('name');

    if ($vendor->save()) return Vendor::find($vendor->id)->toJson();
    return serialize(json_decode($vendor->errors));
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    $post = new Vendor(Input::all());
    $vendor = Vendor::find($post->id);
    $vendor->name = $post->name;
    if ($vendor->save()) return "success";
    return serialize(json_decode($vendor->errors));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $vendor = Vendor::find($id);
    if(empty($vendor)) return "true";
    if($vendor->delete()) return "true";
    return "false";
  }

}