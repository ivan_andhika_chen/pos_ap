/*******************************************
 * @author Ivan Andhika <ivan.andhika@kiranatama.com>
 * April 22, 2013
 * requirement: autoNumeric.js
 *******************************************/

// Load currency
function toCurrency(arrNode,region,sep,sepDec,decDigits){
  region = typeof region == 'undefined' ? 'IDR' : region;
  sep = typeof sep == 'undefined' ? '.' : sep;
  sepDec = typeof sepDec == 'undefined' ? ',' : sepDec;
  decDigits = typeof decDigits == 'undefined' ? 2 : (decDigits>0 ? decDigits : 0) ;
  if ((Math.pow(10,decDigits)-1)<=0)
    {
      max = "99999999999999999999"
    }else{
      max = "99999999999999999999."+(Math.pow(10,decDigits)-1)
    }
  for (var i = arrNode.length - 1; i >= 0; i--) {
    $(arrNode[i]).currency({ 
        region: region,
        thousands: sep,
        decimal: sepDec,
        decimals: decDigits
      });
    if ($(arrNode[i]).is("input")){
      if ( $(arrNode[i]).is('[readonly]') ) { 
      }else{
        $(arrNode[i]).autoNumeric({aSep: sep, aDec: sepDec, vMax: max, aSign: getSymbol(region)+" "});
      }
    }
  }
}


// Convert Currency to Float
// number = currency number, example: "$1,000.00"
// decimal = decimal spliter, example: "."
function toFloat(number,decimal){
  decimal = typeof decimal == 'undefined' ? ',' : decimal;
  if (decimal==","){
    return parseFloat(number.replace(/[^0-9\,\-]+/g,"").replace(",","."));
  }else{
    return parseFloat(number.replace(/[^0-9\.\-]+/g,""));
  }
}

// get symbol of currency
function getSymbol(bc){
  // default
  symbol = '';
  if(bc == 'ALL') symbol = 'Lek';
  if(bc == 'ARS') symbol = '$';
  if(bc == 'AWG') symbol = 'f';
  if(bc == 'AUD') symbol = '$';
  if(bc == 'BSD') symbol = '$';
  if(bc == 'BBD') symbol = '$';
  if(bc == 'BYR') symbol = 'p.';
  if(bc == 'BZD') symbol = 'BZ$';
  if(bc == 'BMD') symbol = '$';
  if(bc == 'BOB') symbol = '$b';
  if(bc == 'BAM') symbol = 'KM';
  if(bc == 'BWP') symbol = 'P';
  if(bc == 'BRL') symbol = 'R$';
  if(bc == 'BND') symbol = '$';
  if(bc == 'CAD') symbol = '$';
  if(bc == 'KYD') symbol = '$';
  if(bc == 'CLP') symbol = '$';
  if(bc == 'CNY') symbol = '&yen;';
  if(bc == 'COP') symbol = '$';
  if(bc == 'CRC') symbol = 'c';
  if(bc == 'HRK') symbol = 'kn';
  if(bc == 'CZK') symbol = 'Kc';
  if(bc == 'DKK') symbol = 'kr';
  if(bc == 'DOP') symbol = 'RD$';
  if(bc == 'XCD') symbol = '$';
  if(bc == 'EGP') symbol = '&pound;';
  if(bc == 'SVC') symbol = '$';
  if(bc == 'EEK') symbol = 'kr';
  if(bc == 'EUR') symbol = '&euro;';
  if(bc == 'FKP') symbol = '&pound;';
  if(bc == 'FJD') symbol = '$';
  if(bc == 'GBP') symbol = '&pound;';
  if(bc == 'GHC') symbol = 'c';
  if(bc == 'GIP') symbol = '&pound;';
  if(bc == 'GTQ') symbol = 'Q';
  if(bc == 'GGP') symbol = '&pound;';
  if(bc == 'GYD') symbol = '$';
  if(bc == 'HNL') symbol = 'L';
  if(bc == 'HKD') symbol = '$';
  if(bc == 'HUF') symbol = 'Ft';
  if(bc == 'ISK') symbol = 'kr';
  if(bc == 'IDR') symbol = 'Rp';
  if(bc == 'IMP') symbol = '&pound;';
  if(bc == 'JMD') symbol = 'J$';
  if(bc == 'JPY') symbol = '&yen;';
  if(bc == 'JEP') symbol = '&pound;';
  if(bc == 'LVL') symbol = 'Ls';
  if(bc == 'LBP') symbol = '&pound;';
  if(bc == 'LRD') symbol = '$';
  if(bc == 'LTL') symbol = 'Lt';
  if(bc == 'MYR') symbol = 'RM';
  if(bc == 'MXN') symbol = '$';
  if(bc == 'MZN') symbol = 'MT';
  if(bc == 'NAD') symbol = '$';
  if(bc == 'ANG') symbol = 'f';
  if(bc == 'NZD') symbol = '$';
  if(bc == 'NIO') symbol = 'C$';
  if(bc == 'NOK') symbol = 'kr';
  if(bc == 'PAB') symbol = 'B/.';
  if(bc == 'PYG') symbol = 'Gs';
  if(bc == 'PEN') symbol = 'S/.';
  if(bc == 'PLN') symbol = 'zl';
  if(bc == 'RON') symbol = 'lei';
  if(bc == 'SHP') symbol = '&pound;';
  if(bc == 'SGD') symbol = '$';
  if(bc == 'SBD') symbol = '$';
  if(bc == 'SOS') symbol = 'S';
  if(bc == 'ZAR') symbol = 'R';
  if(bc == 'SEK') symbol = 'kr';
  if(bc == 'CHF') symbol = 'CHF';
  if(bc == 'SRD') symbol = '$';
  if(bc == 'SYP') symbol = '&pound;';
  if(bc == 'TWD') symbol = 'NT$';
  if(bc == 'TTD') symbol = 'TT$';
  if(bc == 'TRY') symbol = 'TL';
  if(bc == 'TRL') symbol = '&pound;';
  if(bc == 'TVD') symbol = '$';
  if(bc == 'GBP') symbol = '&pound;';
  if(bc == 'USD') symbol = '$';
  if(bc == 'UYU') symbol = '$U';
  if(bc == 'VEF') symbol = 'Bs';
  if(bc == 'ZWD') symbol = 'Z$';

  return symbol
}