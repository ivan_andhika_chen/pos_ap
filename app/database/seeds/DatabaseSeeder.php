<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserTableSeeder');
		$this->call('ItemTableSeeder');
		$this->call('VendorTableSeeder');
		$this->call('ItemCategoryTableSeeder');
		$this->call('TransactionTableSeeder');
		$this->call('CustomersTableSeeder');
		$this->call('UserTypesTableSeeder');
		$this->call('TransactionDetailTableSeeder');
	}

}