<?php

class Transaction extends BaseModel {
  protected $guarded = array();
  public static $rules = array();
  protected $softDelete = true;

  public function employee(){
    return $this->belongsTo('User');
  }

  public function customer(){
    return $this->belongsTo('Customer');
  }

  public function transaction_details(){
    return $this->hasMany('TransactionDetail');
  }

}