<?php

class ItemTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
      DB::table('items')->delete();
      $date = new \DateTime;
      $items = array(
        'id'=>'1',
        'barcode'=>'12345',
        'name'=>'AXISPRO',
        'unit'=>'pack',
        'description'=>'Perdana Axis 1.5GB',
        'item_category_id'=>1,
        'vendor_id'=>1,
        'stock'=>20,
        'real_stock'=>20,
        'vendor_unit_price'=>40000.00,
        'selling_price'=>50000.00,
        'created_at'=> $date,
        'updated_at'=> $date
      );
      // Uncomment the below to run the seeder
      DB::table('items')->insert($items);

      $items = array(
        'id'=>'2',
        'barcode'=>'333',
        'name'=>'TRI',
        'unit'=>'pack',
        'description'=>'Perdana 3 3GB',
        'item_category_id'=>1,
        'vendor_id'=>2,
        'stock'=>10,
        'real_stock'=>10,
        'vendor_unit_price'=>45000.00,
        'selling_price'=>50000.00,
        'created_at'=> $date,
        'updated_at'=> $date
      );
      // Uncomment the below to run the seeder
      DB::table('items')->insert($items);
    }

}