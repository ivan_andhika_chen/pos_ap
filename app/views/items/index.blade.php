@extends('layouts.admin')
@section('style')
  <style type="text/css">
    #item_body tr td input,#item_body tr td select{
      width: 100px;
      margin : 0px;
    }
    #item_body tr td textarea{
      width: 150px;
      height: 100px;
    }
    #item_body tr td.stock input,#item_body tr td.real_stock input{
      width: 40px;
      margin : 0px;
    }
    .actions{
      width: 120px;
    }
  </style>
@stop
@section('main')
  <div class="row-fluid">
    <div class="span9">
      <h3>List Item &nbsp;<button id="add_item" class="btn btn-mini btn-primary" type="button" onClick="createNew();" style="<?= $errors!='[]' ? 'display:none':'' ?>"><i class="icon-pencil icon-white"></i> Add</button></h3>
    </div>
    <div class="span3">
      <h3 class="span9" style="text-align:right;">
        &nbsp;<a id="item" class="btn btn-mini btn-warning" href="/assets/files/template_item.xls"><i class=" icon-download-alt icon-white"></i> Download template</a>
      </h3>
      <h3 class="span3" style="text-align:right;">
        &nbsp;<a id="trash" class="btn btn-mini btn-danger" href="items/trash"><i class="icon-trash icon-white"></i> Trash</a>
      </h3>
    </div>
    <div class="span10 pull-right" >
      {{ Form::open(array('url' => 'items/import', 'files' => true,'style="float:right;"')) }}
      {{ Form::file('file',array('id'=>'upload-file-field')); }}
      {{ Form::submit('Upload', array('class'=>'btn btn-primary','id'=>'button-upload')); }}
      {{ Form::close(); }}
    </div>
  </div>

  {{ Form::open(array('action' => 'ItemsController@store','class'=>'form-horizontal', 'id'=>'form_add_item', 'style'=> $errors=="[]" ? 'display:none':'' )) }}
    <div class="row-fluid">
      <div class="span6">
        <div class="control-group">
          {{ Form::label('new_barcode','Barcode',array('class'=>'control-label'))}}
          <div class="controls">
            {{ Form::text('new_barcode',Input::old('barcode'),array('class'=>'span12 number','placeholder'=>'Scan barcode here'))}}
            <span class="label label-important">{{ $errors->first('barcode') }}</span>
          </div>
        </div>
        <div class="control-group">
          {{ Form::label('new_name','Name',array('class'=>'control-label'))}}
          <div class="controls">
            {{ Form::text('new_name',Input::old('name'),array('class'=>'span12','placeholder'=>'Name of Item'))}}
            <span class="label label-important">{{ $errors->first('name') }}</span>
          </div>
        </div>
        <div class="control-group">
          {{ Form::label('new_description','Description',array('class'=>'control-label'))}}
          <div class="controls">
            {{ Form::text('new_description',Input::old('description'),array('class'=>'span12','placeholder'=>'Description of Item'))}}
            <span class="label label-important">{{ $errors->first('description') }}</span>
          </div>
        </div>
        <div class="control-group">
          {{ Form::label('new_item_category_id','Item Category',array('class'=>'control-label'))}}
          <div class="controls">
            <span class="choose choose_item_category">
              <div class="input-append">
                {{ Form::select('new_item_category_id', $item_category_options,array('class'=>'item_category_id')); }}
                {{ Form::button('<i class="icon-plus icon-white"></i>', array('class'=>'add_item_category btn btn-mini btn-success','style'=>'padding:4px;')) }}
              </div>
            </span>
            <span class="new new_item_category">
              <div class="input-append">
                {{ Form::text('new_item_category',Input::old('item_category'),array('class'=>'span11 item_category_name','placeholder'=>'New Item Category'))}}
                {{ Form::button('<i class="icon-minus icon-white"></i>', array('class'=>'remove_item_category btn btn-mini btn-danger','style'=>'padding:4px;')) }}
              </div>
            </span> 
            <span class="label label-important">{{ $errors->first('item_category_id') }}</span>
          </div>
        </div>
        <div class="control-group">
          {{ Form::label('new_vendor_id','Vendor',array('class'=>'control-label'))}}
          <div class="controls">
            <span class="choose choose_vendor">
              <div class="input-append">
                {{ Form::select('new_vendor_id', $vendor_options,array('class'=>'vendor_id')) }}
                {{ Form::button('<i class="icon-plus icon-white"></i>', array('class'=>'add_item_category btn btn-mini btn-success','style'=>'padding:4px;')) }}
              </div>
            </span>
            <span class="new new_vendor">
              <div class="input-append">
                {{ Form::text('new_vendor',Input::old('vendor'),array('class'=>'span11 vendor_name','placeholder'=>'New Vendor'))}}
                {{ Form::button('<i class="icon-minus icon-white"></i>', array('class'=>'remove_vendor btn btn-mini btn-danger','style'=>'padding:4px;')) }}
              </div>
            </span>
            <span class="label label-important">{{ $errors->first('vendor_id') }}</span>
          </div>
        </div>
      </div>
      <div class="span6">
        <div class="control-group">
          {{ Form::label('new_stock','Stock',array('class'=>'control-label'))}}
          <div class="controls">
            {{ Form::text('new_stock',Input::old('stock'),array('class'=>'span12 number','placeholder'=>'Input New Stock'))}}
            <span class="label label-important">{{ $errors->first('stock') }}</span>
          </div>
        </div>
        <div class="control-group">
          {{ Form::label('new_real_stock','Real Stock',array('class'=>'control-label'))}}
          <div class="controls">
            {{ Form::text('new_real_stock',Input::old('real_stock'),array('class'=>'span12 number','placeholder'=>'Input New Real Stock'))}}
            <span class="label label-important">{{ $errors->first('real_stock') }}</span>
          </div>
        </div>
        <div class="control-group">
          {{ Form::label('new_unit','Unit',array('class'=>'control-label'))}}
          <div class="controls">
            {{ Form::text('new_unit',Input::old('unit'),array('class'=>'span12','placeholder'=>'Input New Unit [all lower case]'))}}
            <span class="label label-important">{{ $errors->first('unit') }}</span>
          </div>
        </div>
        <div class="control-group">
          {{ Form::label('new_vendor_unit_price','Vendor Unit Price',array('class'=>'control-label'))}}
          <div class="controls">
            <div class="input-prepend input-append">
              <span class="add-on">Rp</span>
              {{ Form::text('new_vendor_unit_price',Input::old('vendor_unit_price'),array('class'=>'span12 number','placeholder'=>'Vendor unit price of Item'))}}
              <span class="add-on">.00</span>
              <br/>
              <span class="label label-important">{{ $errors->first('vendor_unit_price') }}</span>
            </div>
          </div>
        </div>
        <div class="control-group">
          {{ Form::label('new_selling_price','Selling Price',array('class'=>'control-label'))}}
          <div class="controls">
            <div class="input-prepend input-append">
              <span class="add-on">Rp</span>
              {{ Form::text('new_selling_price',Input::old('selling_price'),array('class'=>'span12 number','placeholder'=>'Selling Price of Item'))}}
              <span class="add-on">.00</span>
              <br/>
              <span class="label label-important">{{ $errors->first('selling_price') }}</span>
            </div>
          </div>
        </div>
      </div>
      <div class="span12 text-center">
        {{ Form::submit('Save',array('class'=>'btn btn-primary')); }}
        {{ Form::button('Cancel',array('class'=>'btn','onClick'=> 'cancelNew();')); }}
      </div>
    </div>
  {{ Form::close() }}

  <table class="table table-striped table-bordered" style="background:white;">
    <thead>
      <tr>
        <th>No.</th>
        <th>Barcode</th>
        <th>Name</th>
        <th>Description</th>
        <th>Item Category</th>
        <th>Vendor</th>
        <th>Stock</th>
        <th>Real Stock</th>
        <th>Unit</th>
        <th>Vendor Unit Price</th>
        <th>Selling Price</th>
        <th>Created</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody id="item_body">
      @if(count($items)>0)
        @foreach ($items as $key => $item)
          <tr id="row_{{$item->id}}">
            <td class="no"><?=($key+1 + (($items->getCurrentPage()-1)*$items->getPerPage())) ?></td>
            <td class="editable barcode" >
              <span>{{$item->barcode; }}</span>
              {{ Form::text('barcode',$item->barcode,array('placeholder'=>'Fill barcode','class'=>'barcode_'.$item->id,'style'=>'display:none;'))}}
            </td>
            <td class="editable name" >
              <span>{{$item->name; }}</span>
              {{ Form::text('name',$item->name,array('placeholder'=>'Fill name','class'=>'name_'.$item->id,'style'=>'display:none;'))}}
            </td>
            <td class="editable description" >
              <span>{{$item->description; }}</span>
              {{ Form::textarea('description',$item->description,array('placeholder'=>'Fill description','class'=>'description_'.$item->id,'style'=>'display:none;'))}}
            </td>
            <td class="editable item_category" >
              <span>{{$item->item_category()->withTrashed()->first()->name; }}</span>
              {{ Form::select('item_category_id', $item_category_options, $item->item_category()->withTrashed()->first()->name, array('class'=>'item_category_'.$item->id,'style'=>'display:none;')); }}
            </td>
            <td class="editable vendor right-text" >
              <span>{{$item->vendor()->withTrashed()->first()->name; }}</span>
              {{ Form::select('vendor_id', $vendor_options, $item->vendor()->withTrashed()->first()->name, array('class'=>'vendor_'.$item->id,'style'=>'display:none;')) }}
            </td>
            <td class="editable stock right-text" >
              <span>{{$item->stock; }}</span>
              {{ Form::text('stock',$item->stock,array('placeholder'=>'Fill stock','class'=>'number stock_'.$item->id,'style'=>'display:none;'))}}
            </td>
            <td class="editable real_stock right-text" >
              <span>{{$item->real_stock; }}</span>
              {{ Form::text('real_stock',$item->real_stock,array('placeholder'=>'Fill real_stock','class'=>'number real_stock_'.$item->id,'style'=>'display:none;'))}}
            </td>
            <td class="editable real_stock" >
              <span>{{$item->unit; }}</span>
              {{ Form::text('unit',$item->unit,array('placeholder'=>'Fill Unit','class'=>'unit_'.$item->id,'style'=>'display:none;'))}}
            </td>
            <td class="editable vendor_unit_price right-text" >
              <span class="money">{{$item->vendor_unit_price; }}</span>
              {{ Form::text('vendor_unit_price',$item->vendor_unit_price,array('placeholder'=>'Fill vendor_unit_price','class'=>'decimal vendor_unit_price_'.$item->id,'style'=>'display:none;'))}}
            </td>
            <td class="editable selling_price right-text" >
              <span class="money">{{$item->selling_price; }}</span>
              {{ Form::text('selling_price',$item->selling_price,array('placeholder'=>'Fill selling_price','class'=>'decimal selling_price_'.$item->id,'style'=>'display:none;'))}}
            </td>
            <td>{{date("d-m-Y",strtotime($item->created_at)); }}</td>
            <td class="actions" style="min-width:120px;">
              {{ Form::button('<i class="icon-edit icon-white"></i> Edit', array('class'=>'edit btn btn-mini btn-primary')) }}
              {{ Form::button('<i class="icon-remove icon-white"></i> Delete', array('class'=>'delete btn btn-mini btn-danger','onClick'=>"confirmDelete('".$item->id."');")) }}

              {{ Form::button('<i class="icon-ok icon-white"></i> Update', array('class'=>'update btn btn-mini btn-success','style'=>'display:none;','onClick'=>"update('".$item->id."')")) }}
              {{ Form::button('Cancel', array('class'=>'cancel_update btn btn-mini','style'=>'display:none;','onClick'=>"cancelUpdate('".$item->id."');")) }}

              {{ Form::button('<i class="icon-remove icon-white"></i> Delete', array('class'=>'confirmDelete btn btn-mini btn-danger','style'=>'display:none;','onClick'=>"deleteItem('".$item->id."');")) }}
              {{ Form::button('Cancel', array('class'=>'cancelDelete btn btn-mini','style'=>'display:none;','onClick'=>"confirmDelete('".$item->id."');")) }}
            </td>
          </tr>
        @endforeach
      @else
        <tr class="info">
          <td colspan="12"><p class="text-center">No Data</p></td>
        </tr>
      @endif
    </tbody>
  </table>
  {{ $items->links() }}
@stop

@section('script')
  <script type="text/javascript">
    $(document).ready(function(){
      numberOnly(".number")
      numberOnly(".decimal")
      @if (Input::old('item_category'))
        $(".choose_item_category").hide();
        $(".new_item_category").show();
      @else
        $(".choose_item_category").show();
        $(".new_item_category").hide();
      @endif

      @if (Input::old('vendor'))
        $(".choose_vendor").hide();
        $(".new_vendor").show();
      @else
        $(".choose_vendor").show();
        $(".new_vendor").hide();
      @endif

      $("#button-upload").click(function(){
        if ($("#upload-file-field").val()=="") {
          alert("Pilih file terlebih dahulu.");
          return false;
        };
      })
      
    })
    function createNew(){
      $('#form_add_item').slideDown();
      $('#add_item').slideUp();
    }

    function cancelNew(){
      $('#form_add_item').slideUp();
      $('#add_item').slideDown();
    }

    $(".editable").click(function(){
      $(this).children("input").show();
      $(this).children("textarea").show();
      $(this).children("select").show();
      $(this).children("span").hide();
      $(this).siblings(".actions").children('.edit').hide();
      $(this).siblings(".actions").children('.delete').hide();
      $(this).siblings(".actions").children('.update').show();
      $(this).siblings(".actions").children('.cancel_update').show();
    })

    $(".edit").click(function(){
      $(this).parent().siblings(".barcode").children("span").hide();
      $(this).parent().siblings(".name").children("span").hide();
      $(this).parent().siblings(".description").children("span").hide();
      $(this).parent().siblings(".item_category").children("span").hide();
      $(this).parent().siblings(".vendor").children("span").hide();
      $(this).parent().siblings(".stock").children("span").hide();
      $(this).parent().siblings(".real_stock").children("span").hide();
      $(this).parent().siblings(".unit").children("span").hide();
      $(this).parent().siblings(".vendor_unit_price").children("span").hide();
      $(this).parent().siblings(".selling_price").children("span").hide();

      $(this).parent().siblings(".barcode").children("input").show();
      $(this).parent().siblings(".name").children("input").show();
      $(this).parent().siblings(".description").children("textarea").show();
      $(this).parent().siblings(".item_category").children("select").show();
      $(this).parent().siblings(".vendor").children("select").show();
      $(this).parent().siblings(".stock").children("input").show();
      $(this).parent().siblings(".real_stock").children("input").show();
      $(this).parent().siblings(".unit").children("input").show();
      $(this).parent().siblings(".vendor_unit_price").children("input").show();
      $(this).parent().siblings(".selling_price").children("input").show();

      $(this).parent().children('.edit').hide();
      $(this).parent().children('.delete').hide();
      $(this).parent().children('.update').show();
      $(this).parent().children('.cancel_update').show();

    })

    function cancelUpdate(row_id){
      $("#row_"+row_id+" > td").each(function(){
        $(this).children("span").show();
        $(this).children("input").hide();
        $(this).children("textarea").hide();
        $(this).children("select").hide();
      })
        $("#row_"+row_id).children(".actions").children('.edit').show();
        $("#row_"+row_id).children(".actions").children('.delete').show();
        $("#row_"+row_id).children(".actions").children('.update').hide();
        $("#row_"+row_id).children(".actions").children('.cancel_update').hide();
    }

    function updateAjax(row_id){
      $("#row_"+row_id+" > td").each(function(){
        if ($(this).children("input").length >0){
          $(this).children("span").text($(this).children("input").val());
        }
        if ($(this).children("textarea").length >0){
          $(this).children("span").text($(this).children("textarea").val());
        }
        if ($(this).children("select").length >0){
          $(this).children("span").text($(this).children("select").find(":selected").text());
        }
        $(this).children("span").show();
        $(this).children("input").hide();
        $(this).children("textarea").hide();
        $(this).children("select").hide();
      })
      $("#row_"+row_id).children(".actions").children('.edit').show();
      $("#row_"+row_id).children(".actions").children('.delete').show();
      $("#row_"+row_id).children(".actions").children('.update').hide();
      $("#row_"+row_id).children(".actions").children('.cancel_update').hide();
    }

    function update(item_id){
      $.ajax({
          url: '/items/'+item_id,
          type: 'put',
          data: {_token: "{{csrf_token()}}", id : item_id, barcode : $(".barcode_"+item_id).val(), name : $(".name_"+item_id).val(), description : $(".description_"+item_id).val(), item_category_id : $(".item_category_"+item_id).val(), vendor_id : $(".vendor_"+item_id).val(), stock : $(".stock_"+item_id).val(), real_stock : $(".real_stock_"+item_id).val(), unit : $(".unit_"+item_id).val(), vendor_unit_price : $(".vendor_unit_price_"+item_id).val(), selling_price : $(".selling_price_"+item_id).val()},
          success: function (data) {
            if (data=="success") {
              updateAjax(item_id);
              $("#row_"+item_id).css('background-color', '#DFF0D8');
              $("#row_"+item_id).css('color', '#468847');
              $("#row_"+item_id).animate({
                backgroundColor: "FFFFFF",
                color: "#000",
                width: 500
                }, 1000 );
              toCurrency(['.money'])
              showNotification("success", "Item berhasil diubah.");
            }else{
              showNotification("error", "Periksa kembali data yang dimasukkan.");
              console.log("[ERROR]"+"\n"+data);
            };
          },
           error: function(data){
            showNotification("error", "Periksa kembali data yang dimasukkan.");
          }
        });
    }

    function showNotification(type, message){
      notification = ""
      if(type == 'error'){
        notification = "<div class=\"alert alert-error fade in\">" +
                         "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" +
                         "<h4>Error</h4>";
      }else if(type == 'success'){
        notification = "<div class=\"alert alert-success fade in\">" +
                         "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>"
                         "<h4>Success</h4>";
      }
      notification += "<span class=\"message\">"+ message +"</span></div>";
      $('#notif').html(notification);
    }

    function confirmDelete(item_id){
      $("#row_"+item_id).children(".actions").children(".edit").toggle();
      $("#row_"+item_id).children(".actions").children(".delete").toggle();
      $("#row_"+item_id).children(".actions").children(".confirmDelete").toggle();
      $("#row_"+item_id).children(".actions").children(".cancelDelete").toggle();
    }

    function deleteItem(item_id){
      $.ajax({
        url: '/items/'+item_id,
        type: 'delete',
        success: function (data) {
          if(data=="true"){
            $("#row_"+item_id).hide(function(){
              $("#row_"+item_id).remove()
              reindex()
            });
          }
        }
      });
    }

    function reindex(){
      index=1
      $("#item_body > tr").each(function(){
        $(this).children(".no").text(index);
        index++
      })
    }

    $(".add_item_category, .add_vendor").click(function(){
      $(this).parent().parent().hide();
      $(this).parent().parent().siblings(".new").show();
    })
    $(".remove_item_category, .remove_vendor").click(function(){
      $(this).parent().parent().hide();
      $(this).siblings('input').val('');
      $(this).parent().parent().siblings(".choose").show();
    })
</script>
@stop
