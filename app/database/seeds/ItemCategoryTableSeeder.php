<?php

class ItemCategoryTableSeeder extends Seeder {

  public function run()
  {
  	DB::table('item_categories')->delete();
    $date = new \DateTime;
    $items = array(
      'id'=>'1',
      'name'=>'Perdana',
      'created_at'=> $date,
      'updated_at'=> $date
    );
    // Uncomment the below to run the seeder
    DB::table('item_categories')->insert($items);
  }

}