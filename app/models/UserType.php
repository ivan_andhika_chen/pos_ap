<?php

class UserType extends BaseModel {
  protected $guarded = array();
  protected $softDelete = true;
  public static $rules = array();

  public function users()
  {
      return $this->hasMany('User');
  }
}