<?php

class CustomersTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
    	DB::table('customers')->delete();
        $date = new \DateTime;
        $customers = array(
          'id'=>'1',
          'identity_number'=>'1108020',
          'name'=>'Andhika C',
          'address'=>'Seereh 21',
          'phone'=>'0568431',
          'deleted_at'=>null,
          'created_at'=>$date,
          'updated_at'=>$date
        );

        // Uncomment the below to run the seeder
        DB::table('customers')->insert($customers);
    }

}