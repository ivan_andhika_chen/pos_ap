<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="/assets/css/admin.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/flick/jquery-ui-1.10.3.custom.acehill.css" rel="stylesheet" type="text/css">
    @if (!Auth::check())
      <link href="/assets/css/login.css" rel="stylesheet" type="text/css">
    @endif
    <title>Admin Panel - posCloud</title>
    @yield('style')
  </head>

  <body>

      <div id="wrap">
        <!-- Fixed navbar -->
        @if (Auth::check())
          @include('layouts.navbar')
        @endif
        <!-- end Fixed navbar -->
        <div class="container-fluid">
          <!-- notifications -->
          <div id="notif">
            @include('layouts.notifications')
          </div>
          <!-- end notifications -->

          @yield('main')
        </div>
        <div id="push"></div>
      </div>

      <!-- sticky footer -->
        @include('layouts.footer')
      <!-- end sticky footer -->
    <script type="text/javascript" src="/assets/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript" src="/assets/js/customize.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.currency.js"></script>
    <script type="text/javascript" src="/assets/js/autoNumeric.js"></script>
    <script type="text/javascript" src="/assets/js/autoNumeric.currency.js"></script>
    @yield('script')
  </body>
</html>